use core::time;
use std::io::{Read, Write};
use std::net::{TcpListener, TcpStream};
// Received 12 bytes: [1, 0, 0, 0, 0, 6, 1, 3, 3, 234, 0, 6]
fn main() -> Result<(), Box<dyn std::error::Error>> {
    let addr = "127.0.0.1:8888";
    let listener = TcpListener::bind(addr)?;

    println!("Listening on: {}", addr);

    for stream in listener.incoming() {
        match stream {
            Ok(socket) => {
                println!("New connection from: {:?}", socket.peer_addr());
                handle_client(socket)?;
            }
            Err(e) => {
                eprintln!("Error accepting connection: {}", e);
            }
        }
    }

    Ok(())
}

fn handle_client(mut socket: TcpStream) -> Result<(), Box<dyn std::error::Error>> {
    let mut buffer = [0; 1024];

    let mut ss = TcpStream::connect("127.0.0.1:4001").unwrap();
    ss.set_read_timeout(Some(time::Duration::from_secs(5)))
        .expect("Error setting timeout.");

    loop {
        match socket.read(&mut buffer) {
            Ok(0) => {
                // Connection closed
                println!("Connection closed by client");
                break;
            }
            Ok(n) => {
                // Print received data
                println!("Received {} bytes: {:02X?}", n, &buffer[..n]);
                ss.write_all(&buffer[..n])?;

                // Handle Modbus command (you can add your Modbus parsing logic here)
                handle_modbus_command(&buffer[..n])?;

                // Echo back to the client (optional)
                socket.write_all(&buffer[..n])?;
            }
            Err(e) => {
                eprintln!("Error reading from socket: {}", e);
                break;
            }
        }
    }

    Ok(())
}

fn handle_modbus_command(data: &[u8]) -> Result<(), Box<dyn std::error::Error>> {
    // Add your Modbus parsing logic here based on the received data
    // For simplicity, this is just a placeholder function
    println!("Handling Modbus command: {:?}", data);

    Ok(())
}
